<?php 
global $smof_data;
if( !isset($data) ){
	$data = $smof_data;
}

$data = ftc_array_atts(
			array(
				/* FONTS */
				'ftc_body_font_enable_google_font'					=> 1
				,'ftc_body_font_family'								=> "Arial"
				,'ftc_body_font_google'								=> "Montserrat"
				
				,'ftc_secondary_body_font_enable_google_font'		=> 1
				,'ftc_secondary_body_font_family'					=> "Arial"
				,'ftc_secondary_body_font_google'					=> "Raleway"
				
				/* COLORS */
				,'ftc_primary_color'									=> "#cd5f49"

				,'ftc_secondary_color'								=> "#444444"
				
                                ,'ftc_body_background_color'								=> "#ffffff"
				/* RESPONSIVE */
				,'ftc_responsive'									=> 1
				,'ftc_layout_fullwidth'								=> 0
				,'ftc_enable_rtl'									=> 0
				
				/* FONT SIZE */
				/* Body */
				,'ftc_font_size_body'								=> 13
				,'ftc_line_height_body'								=> 20
				
				/* Custom CSS */
				,'ftc_custom_css_code'								=> ''
		), $data);		
		
$data = apply_filters('ftc_custom_style_data', $data);

extract( $data );

/* font-body */
if( $data['ftc_body_font_enable_google_font'] ){
	$ftc_body_font				= $data['ftc_body_font_google']['font-family'] ;
}
else{
	$ftc_body_font				= $data['ftc_body_font_family'];
}

if( $data['ftc_secondary_body_font_enable_google_font'] ){
	$ftc_secondary_body_font		= $data['ftc_secondary_body_font_google']['font-family'] ;
}
else{
	$ftc_secondary_body_font		= $data['ftc_secondary_body_font_family'];
}

?>	
	
	/*
	1. FONT FAMILY
	2. GENERAL COLORS
	*/
	
	
	/* ============= 1. FONT FAMILY ============== */

    body{
        line-height: <?php echo esc_html($ftc_line_height_body)."px"?>;
    }
	
        html, 
	body,.widget-title.heading-title,
        .widget-title.product_title,.newletter_sub_input .button.button-secondary,
        #mega_main_menu.primary ul li .mega_dropdown > li.sub-style > .item_link .link_text
	,  .item-description .product_title, .item-description .price,
	.woocommerce div.product .product_title, .blogs article h3.product_title, .list-posts .post-info .entry-title
	, .content_title p, .newsletter-footer p.submit-footer, .single-post article .post-info .entry-title,
	#comments .comments-title, .text_service p, 
        .woocommerce div.product p.price, 
    .ftc-shop-cart p
	{
		font-family: <?php echo esc_html($ftc_body_font) ?>;
	}
	.widget_recently_viewed_products ul.product_list_widget span.price span
	{
		font-family: <?php echo esc_html($ftc_body_font) ?> !important;
	}
	
	#mega_main_menu.primary ul li .mega_dropdown > li.sub-style > ul.mega_dropdown,
        #mega_main_menu li.multicolumn_dropdown > .mega_dropdown > li .mega_dropdown > li,
        #mega_main_menu.primary ul li .mega_dropdown > li > .item_link .link_text,
        .info-open,
        .info-phone,
        .ftc-sb-account .ftc_login > a,
        .ftc-sb-account,
        .dropdown-button span > span,
        body p,
        .wishlist-empty,
        div.product .social-sharing li a,
        .ftc-search form,
        .conditions-box,
        .testimonial-content .info,
        .testimonial-content .byline,
        .widget-container ul.product-categories ul.children li a,
        .widget-container:not(.ftc-product-categories-widget):not(.widget_product_categories):not(.ftc-items-widget) :not(.widget-title),
        .ftc-products-category ul.tabs li span.title,
        .woocommerce-pagination,
        .woocommerce-result-count,
        .woocommerce .products.list .product h3.product-name > a,
        .woocommerce-page .products.list .product h3.product-name > a,
        .woocommerce .products.list .product .price .amount,
        .woocommerce-page .products.list .product .price .amount,
        .products.list .short-description.list,
        div.product .single_variation_wrap .amount,
        div.product div[itemprop="offers"] .price .amount,
        .orderby-title,
        .blogs .post-info,
        .blog .entry-info .entry-summary .short-content,
        .single-post .entry-info .entry-summary .short-content,
        .single-post article .post-info .info-category,
        #comments .comment-metadata a,
        .post-navigation .nav-previous,
        .post-navigation .nav-next,
        .woocommerce-review-link,
        .ftc_feature_info,
        .woocommerce div.product p.stock,
        .woocommerce div.product .summary div[itemprop="description"],
        .woocommerce div.product .woocommerce-tabs .panel,
        .woocommerce div.product form.cart .group_table td.label,
        .woocommerce div.product form.cart .group_table td.price,
        footer,
        footer a,
        .blogs article .image-eff:before,
        .blogs article a.gallery .owl-item:after,
		.header-language, .header-currency,
 a.ftc-checkout-menu, .custom_content, .woocommerce .product   .item-description .meta_info a
	, .intro-bottom .content_intro, .ftc-breadcrumb-title .ftc-breadcrumbs-content,
	.woocommerce .woocommerce-ordering .orderby, .woocommerce-page .woocommerce-ordering .orderby
	, article a.button-readmore, .contact_info_map .info_contact .info_column ul li,
	.woocommerce div.product div.summary p.cart a, .woocommerce div.product form.cart .button,
	.feature_home1 a.slide-button, .summary .woocommerce-product-details__short-description ,
	.pp_woocommerce div.product form.cart .group_table td.label,
	.pp_woocommerce div.product form.cart .button,
	.ftc-enable-ajax-search .ftc-search-meta.item-description a.product_title,
	.ftc-enable-ajax-search .ftc-search-meta .price,
	.vcard.author, .caftc-link, .tags-link, .date-time.date-time-meta, .full-content{
		font-family: <?php echo esc_html($ftc_secondary_body_font) ?>;
	}
	body,
        .site-footer,
        .woocommerce div.product form.cart .group_table td.label,
        .woocommerce .product .conditions-box span,
         .item-description .meta_info .yith-wcwl-add-to-wishlist a,  .item-description .meta_info .compare,
        .info-company li i,
        .social-icons .ftc-tooltip:before,
        .tagcloud a,
        .details_thumbnails .owl-nav > div:before,
        div.product .summary .yith-wcwl-add-to-wishlist a:before,
        .pp_woocommerce div.product .summary .compare:before,
        .woocommerce div.product .summary .compare:before,
        .woocommerce-page div.product .summary .compare:before,
        .woocommerce #content div.product .summary .compare:before,
        .woocommerce-page #content div.product .summary .compare:before,
        .woocommerce div.product form.cart .variations label,
        .woocommerce-page div.product form.cart .variations label,
        .pp_woocommerce div.product form.cart .variations label,
        blockquote,
        .ftc-number h3.ftc_number_meta,
        .woocommerce .widget_price_filter .price_slider_amount,
        .wishlist-empty,
        .woocommerce div.product form.cart .button,
        .woocommerce table.wishlist_table
        {
                font-size: <?php echo esc_html($ftc_font_size_body) ?>px;
        }
	/* ========== 2. GENERAL COLORS ========== */
        /* ========== Primary color ========== */
	.header-currency:hover .ftc-currency > a,
        .ftc-sb-language:hover li .ftc_lang,
        .woocommerce a.remove:hover,
        .dropdown-container .ftc_cart_check > a.button.view-cart:hover,
        .ftc-my-wishlist a:hover,
        .ftc-sb-account .ftc_login > a:hover,
        .header-currency .ftc-currency ul li:hover,
        .dropdown-button span:hover,
        body.wpb-js-composer .vc_general.vc_tta-tabs .vc_tta-tab.vc_active > a,
        body.wpb-js-composer .vc_general.vc_tta-tabs .vc_tta-tab > a:hover,
        #mega_main_menu.primary > .menu_holder.sticky_container > .menu_inner > ul > li > .item_link:hover *,
        #mega_main_menu.primary > .menu_holder.sticky_container > .menu_inner > ul > li.current-menu-item > .item_link *,
        #mega_main_menu.primary > .menu_holder > .menu_inner > ul > li.current-menu-ancestor > .item_link,
        #mega_main_menu.primary > .menu_holder > .menu_inner > ul > li.current-menu-ancestor > .item_link *,
        #mega_main_menu.primary > .menu_holder > .menu_inner > ul > li:hover > .item_link *,
        #mega_main_menu.primary .mega_dropdown > li > .item_link:hover *,
        #mega_main_menu.primary .mega_dropdown > li.current-menu-item > .item_link *,
        #mega_main_menu.primary > .menu_holder > .menu_inner > ul > li.current-menu-item > .item_link *,
        .woocommerce .products .product .price,
        .woocommerce div.product p.price,
        .woocommerce div.product span.price,
        .woocommerce .products .star-rating,
        .woocommerce-page .products .star-rating,
        .star-rating:before,
        div.product div[itemprop="offers"] .price .amount,
        div.product .single_variation_wrap .amount,
        .pp_woocommerce .star-rating:before,
        .woocommerce .star-rating:before,
        .woocommerce-page .star-rating:before,
        .woocommerce-product-rating .star-rating span,
        ins .amount,
        .ftc-meta-widget .price ins,
        .ftc-meta-widget .star-rating,
        .ul-style.circle li:before,
        .woocommerce form .form-row .required,
        .blogs .comment-count i,
        .blog .comment-count i,
        .single-post .comment-count i,
        .single-post article .post-info .info-category .cat-links a,
        .single-post article .post-info .info-category .vcard.author a,
        .ftc-breadcrumb-title .ftc-breadcrumbs-content,
        .ftc-breadcrumb-title .ftc-breadcrumbs-content span.current,
        .ftc-breadcrumb-title .ftc-breadcrumbs-content a:hover,
        .grid_list_nav a.active,
        .ftc-quickshop-wrapper .owl-nav > div.owl-next:hover,
        .ftc-quickshop-wrapper .owl-nav > div.owl-prev:hover,
        .shortcode-icon .vc_icon_element.vc_icon_element-outer .vc_icon_element-inner.vc_icon_element-color-orange .vc_icon_element-icon,
        .comment-reply-link .icon,
        body table.compare-list tr.remove td > a .remove:hover:before,
        a:hover,
        a:focus,
        .vc_toggle_title h4:hover,
        .vc_toggle_title h4:before,
        .blogs article h3.product_title a:hover,
        article .post-info a:hover,
        article .comment-content a:hover,
        .main-navigation li li.focus > a,
	.main-navigation li li:focus > a,
	.main-navigation li li:hover > a,
	.main-navigation li li a:hover,
	.main-navigation li li a:focus,
	.main-navigation li li.current_page_item a:hover,
	.main-navigation li li.current-menu-item a:hover,
	.main-navigation li li.current_page_item a:focus,
	.main-navigation li li.current-menu-item a:focus,.woocommerce-account .woocommerce-MyAccount-navigation li.is-active a, article .post-info .cat-links a,article .post-info .tags-link a,
    .vcard.author a,article .entry-header .caftc-link .cat-links a,.woocommerce-page .products.list .product h3.product-name a:hover,
    .woocommerce .products.list .product h3.product-name a:hover,
	.tp-leftarrow.tparrows:before, #mega_main_menu.primary .mega_dropdown > li.current-menu-item > .item_link *, 
	#mega_main_menu.primary .mega_dropdown > li > .item_link:focus *,
	#mega_main_menu.primary .mega_dropdown > li > .item_link:hover *,
	#mega_main_menu.primary li.post_type_dropdown > .mega_dropdown > li > .processed_image:hover > .cover > a > i,
	.ftc-shoppping-cart a.ftc_cart:hover, .header-currency ul li:hover,
.header-language a.lang_sel_sel.icl-en:hover, .header-currency a.wcml_selected_currency:hover,
ul.product_list_widget li > a:hover, h3.product-name > a:hover	,
.intro-box:hover, .owl-nav .owl-next,.counter-wrapper > div .number-wrapper .number,
.newsletter-footer input[type="submit"]:hover, footer a:hover, .ftc-footer .copy-com a:hover,
.ftc-sidebar > .widget-container.ftc-product-categories-widget .ftc-product-categories-list ul li.active > a, 
.ftc-sidebar > .widget-container.ftc-product-categories-widget .ftc-product-categories-list ul li > a:hover
, .widget-container ul > li a:hover, .widget-container.widget_categories ul li:hover,
.contact_info_map ul li a:hover, .woocommerce-info::before , .woocommerce-message::before,
span.author:hover, .comment-meta a:hover, a.comment-edit-link:hover,
.footer-mobile > div > a:hover,.footer-mobile > div .ftc-my-wishlist:hover *,
.footer-mobile > div .ftc-my-wishlist i,.footer-mobile > div > a i,
.ftc-shop-cart .ftc-shoppping-cart .dropdown-container .woocommerce-Price-amount,
.woocommerce table.shop_table td .woocommerce-Price-amount,
.ftc-shop-cart .ftc-shoppping-cart a.ftc_cart:hover,
.widget_recently_viewed_products ul.product_list_widget span.price .woocommerce-Price-amount,
.ftc-mobile-wrapper ul#mega_main_menu_ul > li.menu-item-has-children > a.item_link:hover span.link_text:before,
.ftc-mobile-wrapper .ftc-search button.search-button
         {
                color: <?php echo esc_html($ftc_primary_color) ?>;
        }
		.woocommerce a.remove:hover, body table.compare-list tr.remove td > a .remove:hover:before
		{
			color: <?php echo esc_html($ftc_primary_color) ?> !important;
		}
        .dropdown-container .ftc_cart_check > a.button.checkout:hover,
        .woocommerce .widget_price_filter .price_slider_amount .button:hover,
        .woocommerce-page .widget_price_filter .price_slider_amount .button:hover,
        body input.wpcf7-submit:hover,
        .woocommerce .products.list .product   .item-description .add-to-cart a:hover,
        .woocommerce .products.list .product   .item-description .button-in a:hover,
        .woocommerce .products.list .product   .item-description .meta_info  a:not(.quickview):hover,
        .woocommerce .products.list .product   .item-description .quickview i:hover,
        .counter-wrapper > div,
        .tp-bullets .tp-bullet:after,
        .woocommerce .product .conditions-box .onsale,
        .woocommerce #respond input#submit:hover, 
        .woocommerce a.button:hover,
        .woocommerce button.button:hover, 
        .woocommerce input.button:hover,
        .woocommerce .products .product  .images .button-in:hover a:hover,
        .woocommerce .products .product  .images a:hover,
        .vc_color-orange.vc_message_box-solid,
        .woocommerce nav.woocommerce-pagination ul li span.current,
        .woocommerce-page nav.woocommerce-pagination ul li span.current,
        .woocommerce nav.woocommerce-pagination ul li a.next:hover,
        .woocommerce-page nav.woocommerce-pagination ul li a.next:hover,
        .woocommerce nav.woocommerce-pagination ul li a.prev:hover,
        .woocommerce-page nav.woocommerce-pagination ul li a.prev:hover,
        .woocommerce nav.woocommerce-pagination ul li a:hover,
        .woocommerce-page nav.woocommerce-pagination ul li a:hover,
        .woocommerce .form-row input.button:hover,
        .load-more-wrapper .button:hover,
        body .vc_general.vc_tta-tabs.vc_tta-tabs-position-left .vc_tta-tab:hover,
        body .vc_general.vc_tta-tabs.vc_tta-tabs-position-left .vc_tta-tab.vc_active,
        .woocommerce div.product form.cart .button:hover,
        .woocommerce div.product div.summary p.cart a:hover,
        div.product .summary .yith-wcwl-add-to-wishlist a:hover,
        .woocommerce #content div.product .summary .compare:hover,
        div.product .social-sharing li a:hover,
        .woocommerce div.product .woocommerce-tabs ul.tabs li.active,
        .tagcloud a:hover,
        .woocommerce .wc-proceed-to-checkout a.button.alt:hover,
        .woocommerce .wc-proceed-to-checkout a.button:hover,
        .woocommerce-cart table.cart input.button:hover,
        .owl-dots > .owl-dot span:hover,
        .owl-dots > .owl-dot.active span,
        footer .style-3 .newletter_sub .button.button-secondary.transparent,
        .woocommerce .widget_price_filter .ui-slider .ui-slider-range,
        body .vc_tta.vc_tta-accordion .vc_tta-panel.vc_active .vc_tta-panel-title > a,
        body .vc_tta.vc_tta-accordion .vc_tta-panel .vc_tta-panel-title > a:hover,
        body div.pp_details a.pp_close:hover:before,
        .vc_toggle_title h4:after,
        body.error404 .page-header a,
        body .button.button-secondary,
        .pp_woocommerce div.product form.cart .button,
        .shortcode-icon .vc_icon_element.vc_icon_element-outer .vc_icon_element-inner.vc_icon_element-background-color-orange.vc_icon_element-background,
        .style1 .ftc-countdown .counter-wrapper > div,
        .style2 .ftc-countdown .counter-wrapper > div,
        .style3 .ftc-countdown .counter-wrapper > div,
        #cboxClose:hover,
        body > h1,
        table.compare-list .add-to-cart td a:hover,
        .vc_progress_bar.wpb_content_element > .vc_general.vc_single_bar > .vc_bar,
        div.product.vertical-thumbnail .details-img .owl-controls div.owl-prev:hover,
        div.product.vertical-thumbnail .details-img .owl-controls div.owl-next:hover,
        ul > .page-numbers.current,
        ul > .page-numbers:hover,
        article a.button-readmore:hover,.text_service a,.vc_toggle_title h4:before,.vc_toggle_active .vc_toggle_title h4:before,
        .post-item.sticky .post-info .entry-info .sticky-post,
        .woocommerce .products.list .product   .item-description .compare.added:hover
        , a.slide-button span, .tp-rightarrow.tparrows,
		a.slide-button:hover:after, .logo-wrapper, .logo-wrapper:before,
		button.search-button, .text_for_men.row1 h3:after,
		.text_for_women.row2 h3:after ,
		.woocommerce .product .item-description .meta_info a.quickview:hover i,
		.woocommerce .product .item-description .meta_info .yith-wcwl-add-to-wishlist a:hover,
        .ftc-meta-widget.item-description .meta_info a:hover,
        .ftc-meta-widget.item-description .meta_info .yith-wcwl-add-to-wishlist:hover,
.ftc-product .item-description .add-to-cart a, .lastest-product .vc_tta-container h2:before	,
.lastest-product .vc_tta-container h2, .woocommerce .product  .item-description .meta_info .add-to-cart a:first-child:hover:after, 
.ftc-meta-widget.item-description .meta_info .add-to-cart a:first-child:hover:after,
.owl-nav .owl-prev, article a.button-readmore, article a.button-readmore:hover:after
		, body .subscribe_comingsoon input[type="submit"]:hover, #to-top a,
		.page-numbers.current, a.page-numbers:hover, .single-post .form-submit input[type="submit"]:hover
		, .woocommerce div.product div.summary p.cart a, .woocommerce div.product form.cart .button,
		.woocommerce div.product div.summary p.cart a:hover:after, 
		.woocommerce div.product form.cart .button:hover:after,
		.details_thumbnails .owl-nav .owl-prev:hover, .details_thumbnails .owl-nav .owl-next:hover,
		.woocommerce div.product .woocommerce-tabs ul.tabs li:hover,
		.site-content .related.products h2 .bg-heading,
		.ftc-quickshop-wrapper .owl-nav > div.owl-next:hover, 
		.ftc-quickshop-wrapper .owl-nav > div.owl-prev:hover,
		.pp_woocommerce div.product form.cart .button:hover:after,
		a.slide-button, 
		.home3 article a.button-readmore:after, 
		.home3 .woocommerce .product .item-description .meta_info .add-to-cart a:first-child:after,
		.home3 .ftc-product .item-description .add-to-cart a:hover ,
		.home3 .woocommerce .product .item-description .meta_info a.added_to_cart.wc-forward:after,
		#today,.woocommerce .product .item-description .meta_info a.compare:hover i,
        .newsletterpopup form > div .submit-footer input[type="submit"],
        .woocommerce #respond input#submit.alt:hover, .woocommerce a.button.alt:hover, .woocommerce button.button.alt:hover, .woocommerce input.button.alt:hover,
        a.ftc_cart:hover .cart-number p.count-number,
        .load-more-wrapper .button,
        article a.button.load-more:hover:after,
        .text_service a:hover::after,body.error404 .page-header a:hover::after
        {
                background-color: <?php echo esc_html($ftc_primary_color) ?>;
        }
		.tp-caption.rev-btn.rev-withicon, .tp-caption.rev-btn:hover i,
		.slider-home3 .tp-caption.rev-btn.rev-withicon:hover
		{
			background-color: <?php echo esc_html($ftc_primary_color) ?> !important;
		}
	.dropdown-container .ftc_cart_check > a.button.view-cart:hover,
        .dropdown-container .ftc_cart_check > a.button.checkout:hover,
        .woocommerce .widget_price_filter .price_slider_amount .button:hover,
        .woocommerce-page .widget_price_filter .price_slider_amount .button:hover,
        body input.wpcf7-submit:hover,
        .counter-wrapper > div,
        .woocommerce .products .product:hover ,
        .woocommerce-page .products .product:hover ,
        #right-sidebar .product_list_widget:hover li,
        .woocommerce .product   .item-description .meta_info a:hover,
        .woocommerce-page .product   .item-description .meta_info a:hover,
        .ftc-meta-widget.item-description .meta_info a:hover,
        .ftc-meta-widget.item-description .meta_info .yith-wcwl-add-to-wishlist a:hover,
        .woocommerce .products .product:hover ,
        .woocommerce-page .products .product:hover ,
        .ftc-products-category ul.tabs li:hover,
        .ftc-products-category ul.tabs li.current,
        body .vc_tta.vc_tta-accordion .vc_tta-panel.vc_active .vc_tta-panel-title > a,
        body .vc_tta.vc_tta-accordion .vc_tta-panel .vc_tta-panel-title > a:hover,
         body div.pp_details a.pp_close:hover:before,
        .wpcf7 p input:focus,
        .wpcf7 p textarea:focus,
        .woocommerce form .form-row .input-text:focus,
        body .button.button-secondary,
        .ftc-quickshop-wrapper .owl-nav > div.owl-next:hover,
        .ftc-quickshop-wrapper .owl-nav > div.owl-prev:hover,
        #cboxClose:hover, .woocommerce-account .woocommerce-MyAccount-navigation li.is-active,
        .ftc-product-items-widget .ftc-meta-widget.item-description .meta_info .compare:hover,
        .ftc-product-items-widget .ftc-meta-widget.item-description .meta_info .add_to_cart_button a:hover,
        .woocommerce .product   .item-description .meta_info .add-to-cart a:hover,
        .ftc-meta-widget.item-description .meta_info .add-to-cart a:hover 
        , .woocommerce .widget_layered_nav ul li a:hover:before,
		.woocommerce-page .widget_layered_nav ul li a:hover:before{
                border-color: <?php echo esc_html($ftc_primary_color) ?>;
        }
        #ftc_language ul ul,
        .header-currency ul,
        .ftc-account .dropdown-container,
        .ftc-shop-cart .dropdown-container,
        #mega_main_menu.primary > .menu_holder > .menu_inner > ul > li.current_page_item,
        #mega_main_menu > .menu_holder > .menu_inner > ul > li:hover,
        #mega_main_menu.primary > .menu_holder > .menu_inner > ul > li.current-menu-ancestor > .item_link,
        #mega_main_menu > .menu_holder > .menu_inner > ul > li.current_page_item > a:first-child:after,
        #mega_main_menu > .menu_holder > .menu_inner > ul > li > a:first-child:hover:before,
        #mega_main_menu.primary > .menu_holder > .menu_inner > ul > li.current-menu-ancestor > .item_link:before,
        #mega_main_menu.primary > .menu_holder > .menu_inner > ul > li.current_page_item > .item_link:before,
        #mega_main_menu.primary > .menu_holder > .menu_inner > ul > li > .mega_dropdown,
        .woocommerce .product .conditions-box .onsale:before,
        .woocommerce .product .conditions-box .featured:before,
        .woocommerce .product .conditions-box .out-of-stock:before, a.slide-button span:after,
		.logo-wrapper:after,.header-language ul ul, button.search-button:before,
		.lastest-product .vc_tta-container h2:after, .owl-nav .owl-prev:after,
		.woocommerce-info, .site-content .related.products h2 .bg-heading :after,
		.woocommerce-message, .ftc-shoppping-cart:before
        {
                border-top-color: <?php echo esc_html($ftc_primary_color) ?>;
        }
        .woocommerce .products.list .product:hover  .item-description:after,
        .woocommerce-page .products.list .product:hover  .item-description:after
        {
                border-left-color: <?php echo esc_html($ftc_primary_color) ?>;
        }
        footer#colophon .ftc-footer .widget-title:before,
        .woocommerce div.product .woocommerce-tabs ul.tabs,
        #customer_login h2 span:before,
        .cart_totals  h2 span:before,
		.tp-rightarrow.tparrows:after,
a.slide-button:hover span:before, .woocommerce .product .item-description .meta_info .add-to-cart a:first-child:hover:before
        , article a.button-readmore:hover:before,.woocommerce div.product div.summary p.cart a:hover:before, 
		.woocommerce div.product form.cart .button:hover:before,
		.pp_woocommerce div.product form.cart .button:hover:before,
		a.slide-button:hover:before, .tp-caption.rev-btn:hover i:after,
		.home3 article a.button-readmore:before,
		.home3 .woocommerce .product .item-description .meta_info .add-to-cart a:first-child:before,
.home3 .woocommerce .product .item-description .meta_info a.added_to_cart.wc-forward:before,
        .newsletterpopup .textwidget > p.text-popup::after,
        article a.button.load-more:hover:before,
        .text_service a:hover::before,body.error404 .page-header a:hover::before
		{
                border-bottom-color: <?php echo esc_html($ftc_primary_color) ?>;
        }
        
        /* ========== Secondary color ========== */
        body,
        #mega_main_menu.primary ul li .mega_dropdown > li.sub-style > .item_link .link_text,
        .woocommerce a.remove,
        body.wpb-js-composer .vc_general.vc_tta-tabs.vc_tta-tabs-position-left .vc_tta-tab,
        .woocommerce .products .star-rating.no-rating,
        .woocommerce-page .products .star-rating.no-rating,
        .star-rating.no-rating:before,
        .pp_woocommerce .star-rating.no-rating:before,
        .woocommerce .star-rating.no-rating:before,
        .woocommerce-page .star-rating.no-rating:before,
        .woocommerce .product .images .group-button-product > div a,
        .woocommerce .product .images .group-button-product > a, 
        .vc_progress_bar .vc_single_bar .vc_label,
        .vc_btn3.vc_btn3-size-sm.vc_btn3-style-outline,
        .vc_btn3.vc_btn3-size-sm.vc_btn3-style-outline-custom,
        .vc_btn3.vc_btn3-size-md.vc_btn3-style-outline,
        .vc_btn3.vc_btn3-size-md.vc_btn3-style-outline-custom,
        .vc_btn3.vc_btn3-size-lg.vc_btn3-style-outline,
        .vc_btn3.vc_btn3-size-lg.vc_btn3-style-outline-custom,
        .style1 .ftc-countdown .counter-wrapper > div .countdown-meta,
        .style2 .ftc-countdown .counter-wrapper > div .countdown-meta,
        .style3 .ftc-countdown .counter-wrapper > div .countdown-meta,
        .style4 .ftc-countdown .counter-wrapper > div .number-wrapper .number,
        .style4 .ftc-countdown .counter-wrapper > div .countdown-meta,
        body table.compare-list tr.remove td > a .remove:before,
        .woocommerce-page .products.list .product h3.product-name a,
		button.search-button:hover, button.search-button:focus
        , .intro-box
		{
                color: <?php echo esc_html($ftc_secondary_color) ?>;
        }
        .dropdown-container .ftc_cart_check > a.button.checkout,
        .info-company li i,
        body .button.button-secondary:hover,
        body div.ftc-size_chart .pp_close:before,
        div.pp_default .pp_close, body div.pp_woocommerce.pp_pic_holder .pp_close,
        body div.ftc-product-video.pp_pic_holder .pp_close,
        body .ftc-lightbox.pp_pic_holder a.pp_close,
        #cboxClose, #to-top a:hover
        {
                background-color: <?php echo esc_html($ftc_secondary_color) ?>;
        }
        .dropdown-container .ftc_cart_check > a.button.checkout,
        body .button.button-secondary:hover,
        #cboxClose
        {
                border-color: <?php echo esc_html($ftc_secondary_color) ?>;
        }
        
        /* ========== Body Background color ========== */
        body
        {
                background-color: <?php echo esc_html($ftc_body_background_color) ?>;
        }
	