<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'ube1712212035017' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'zW0W7-E431^8s Q%|tPF}4y9 f]$J#{hR5z/fg_`m8}njracLy5@@^xR>)kC).0Q' );
define( 'SECURE_AUTH_KEY',  '[_H#6b+N_mbd:Gk4tWSRl?;ge7R<9=&q|^.65zbcw5;8AyUlX=FY3jHXl<m$3 f!' );
define( 'LOGGED_IN_KEY',    'ErXD{j^jC^j:-kr~Gf!hgoveQ!:ke.1`6(+z}l|}r -F59_C+P_NC{G_`I/`v_e&' );
define( 'NONCE_KEY',        'Y5nj+D;=JXiXI7YU2kkcFoiZGOY(*F.[>7v>H0i-]q#f0z/ef/C</wt:f3?jRgwu' );
define( 'AUTH_SALT',        'Q)];o*_Cadp5Ith[l4ozpByCb{ldxpDO0k;4;k4:[P8~Sfp> X&&ks/k3>n@rD$i' );
define( 'SECURE_AUTH_SALT', 'eo*]>zue%@~GgFW<X#]VfhSik4{}mLBj~8%2UvVdubLXsVd@zK5%e5k*u$ld=PgM' );
define( 'LOGGED_IN_SALT',   'Aod)Ns!&Hfs,y<G5=.5VBjIk7n7Gv<0?:f%>**:$:lx<*lP+:DEm=j4#<u?7q,2;' );
define( 'NONCE_SALT',       'A3-G&!ow#F83+Kn8PgQCOh$6DdIg<MQ/A4n=!V0u+ZX?teaI5@$j#6u-FIT/VZT+' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
